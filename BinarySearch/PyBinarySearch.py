import math

def func(x):
    y = math.sin(x) - x - 1
    return y
e = 1e-4

left = int(input('left border: '))
right = int(input('right border: '))
mid = (left + right) / 2


while math.fabs(left - right) >= e:

    if func(left) * func(mid) < 0:
        right = mid
    else:
        left = mid
    print(mid)
    mid = (left + right) / 2


print('Корень:', mid)