list1 = [x * x for x in range(11)]
list2 = [x * x * x for x in range(5)]

list1.append(100000)
list2.append(100000)
print(' '.join(str(x) for x in list1))
print(' '.join(str(x) for x in list2))
list3 = []
count1 = 0
count2 = 0
for i in range(16):
    if list1[count1] <= list2[count2]:
        list3.append(list1[count1])
        count1 += 1
    else:
        list3.append(list2[count2])
        count2 += 1

print(' '.join(str(x) for x in list3))