def Power(num, pow):
    result = 1
    while pow > 0:
        if pow % 2 == 1:
            result *= num
        num *= num
        pow //= 2
    return result

a = int(input())
deg = int(input())
print(Power(a, deg))

# a = int(input())
# n = int(input())
# def power(a, n):
#     if (n == 0):
#         return 1
#     elif (n % 2 == 1):
#         return power(a, (n -1)) * a
#     else:
#         b = power(a, n // 2)
#         return b * b
#
# print(power(a, n))